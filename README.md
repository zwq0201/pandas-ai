# pandas-ai适配鲲鹏CPU

本项目致力于实现pandas-ai模型在华为鲲鹏CPU上的推理演示

第一步：从github上拉取代码仓库

第二步：根据github上的提示去安装配置环境

第三步：从华为云上购买GaussDB数据库实例，创建数据库、表；往表里添加你所需要的数据

第四步：连接数据库、编写脚本实现pandas-ai功能

数据库显示：

![image.png](https://raw.gitcode.com/zwq0201/pandas-ai/attachment/uploads/27f118a7-61fc-42fe-a524-5653fb9cdf16/image.png 'image.png')

结果展示1：销售额前5的国家
![image.png](https://raw.gitcode.com/zwq0201/pandas-ai/attachment/uploads/edbc884f-546e-4824-b9d5-79a945211519/image.png 'image.png')
结果展示2：销售额前3的总和
![image.png](https://raw.gitcode.com/zwq0201/pandas-ai/attachment/uploads/355ab1ca-ddc8-4f36-af81-035881f5e29d/image.png 'image.png')
